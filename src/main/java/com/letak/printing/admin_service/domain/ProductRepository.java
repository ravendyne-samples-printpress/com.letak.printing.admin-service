package com.letak.printing.admin_service.domain;

import org.springframework.data.repository.CrudRepository;

import com.letak.printing.domain_model.entity.Product;

public interface ProductRepository extends CrudRepository<Product, Integer> {

}
