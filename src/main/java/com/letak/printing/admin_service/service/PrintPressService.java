package com.letak.printing.admin_service.service;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.letak.printing.admin_service.domain.PrintPressRepository;
import com.letak.printing.domain_model.dto.printpress.PrintPressBaseDto;
import com.letak.printing.domain_model.entity.PrintPress;

@RestController
public class PrintPressService {
    
    @Autowired
    PrintPressRepository printPressRepository;

    @RequestMapping("/printers")
    List<PrintPressBaseDto> getAllPrinters() {
        
        List<PrintPressBaseDto> resultList = new ArrayList<PrintPressBaseDto>();

        Iterable<PrintPress> allPPs = printPressRepository.findAll();
        
        allPPs.forEach((pp) -> {
            resultList.add( new PrintPressBaseDto( pp ) );
        });
        
        return resultList;
    }

    @RequestMapping(value = "export/{uuid}", method = RequestMethod.GET)
    @ResponseBody
    public ResponseEntity<ByteArrayResource> exportTransactionsForAccount(@PathVariable("uuid") String uuid) throws IOException
    {
        ByteArrayOutputStream json = new ByteArrayOutputStream();

        List<PrintPress> pp = printPressRepository.findByUuid(uuid);
        
        if(pp.isEmpty()) {

            json.write("{}".getBytes());
            return responseEntityForJsonDownload(json, "empty.json", HttpStatus.NOT_FOUND);
//            return ResponseEntity.notFound().build();
        }

        ObjectMapper mapper = new ObjectMapper();
        mapper.writeValue(json, pp.get(0));

        SimpleDateFormat sdf = new SimpleDateFormat("yyyy.MM.dd-HH.mm.ss");
        ResponseEntity<ByteArrayResource> response = responseEntityForJsonDownload(json, "printpress-" + uuid + "-" + sdf.format(new Date()) + ".json", HttpStatus.OK);

        return response;
    }

    private ResponseEntity<ByteArrayResource> responseEntityForJsonDownload(ByteArrayOutputStream json, String attachmentName, HttpStatus status)
    {
        ByteArrayResource body = new ByteArrayResource(json.toByteArray());

        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.TEXT_PLAIN);
        headers.setContentLength(body.contentLength());
        headers.setContentDispositionFormData("attachment", attachmentName);

        ResponseEntity<ByteArrayResource> response = new ResponseEntity<ByteArrayResource>(body, headers, status);
        return response;
    }

}
