package com.letak.printing.admin_service.service.sandbox;

import java.util.List;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.letak.printing.admin_service.service.ServiceReponse;

@RestController
public class Sandbox {

    @RequestMapping(value="/test", method = RequestMethod.POST)
    ServiceReponse test(@RequestBody List<TestPojo> data) {
        ServiceReponse response = new ServiceReponse();
        
        response.setResult("ok");
        response.setData(data);
        
        return response;
    }

}
