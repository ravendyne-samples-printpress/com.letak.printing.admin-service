package com.letak.printing.admin_service.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.letak.printing.admin_service.domain.ProductRepository;
import com.letak.printing.domain_model.dto.product.ProductWithChildrenDto;
import com.letak.printing.domain_model.entity.Product;

@RestController
public class ProductService {
    
    @Autowired
    ProductRepository productRepository;

    @RequestMapping("/product/{id}")
    ProductWithChildrenDto getProductDetails(@PathVariable("id") Integer id) {
        
        Optional<Product> prod = productRepository.findById(id);
        
        if(!prod.isPresent()) {
            // throw exception
        }
        
        return new ProductWithChildrenDto(prod.get());
    }

}
