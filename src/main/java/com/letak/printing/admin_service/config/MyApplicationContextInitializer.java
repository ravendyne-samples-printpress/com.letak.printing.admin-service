package com.letak.printing.admin_service.config;

import org.springframework.context.ApplicationContextInitializer;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.core.env.MutablePropertySources;

public class MyApplicationContextInitializer implements ApplicationContextInitializer<ConfigurableApplicationContext> {

	@Override
	public void initialize(ConfigurableApplicationContext applicationContext) {
		ConfigurableEnvironment appEnvironment = applicationContext.getEnvironment();
		MutablePropertySources sources = appEnvironment.getPropertySources();
		MyRandomValuePropertySource myRandomPropertySource = new MyRandomValuePropertySource("myrandom");
		sources.addLast(myRandomPropertySource);
	}

}
