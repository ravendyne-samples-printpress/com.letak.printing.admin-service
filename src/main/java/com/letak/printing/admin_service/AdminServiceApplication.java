package com.letak.printing.admin_service;

import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;

import com.letak.printing.admin_service.config.MyApplicationContextInitializer;

@EnableDiscoveryClient
@SpringBootApplication
@EntityScan(basePackageClasses = {
        com.letak.printing.domain_model.Package.class, 
        com.letak.printing.admin_service.domain.Package.class
        })
public class AdminServiceApplication {

	public static void main(String[] args) {

	    SpringApplicationBuilder builder = new SpringApplicationBuilder(AdminServiceApplication.class);

        builder.initializers(new MyApplicationContextInitializer());

        builder.run(args);
	}
}
