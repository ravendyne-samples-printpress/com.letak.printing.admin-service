## PrintPress Admin Backend Microservice

Administration backend API provider.

### Dependencies

- [domain model](https://gitlab.com/ravendyne-samples-printpress/com.letak.printing.domain-model)
